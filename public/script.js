// Configuration

const flagChar = '🏳️'
const bombChar = '💣'
const correctFlagChar = '🏴'
const clockChar = '⏱️'
const gameWonChar = '😎'
const gameOverChar = '💩'

// Helpers

function delegateEvent(parentNode, event, selector, handlerFn) {
  parentNode.addEventListener(event, event => {
    let targetNode = event.target
    while (targetNode != parentNode && targetNode != null) {
      if (targetNode.matches(selector)) {
        handlerFn(event, targetNode)
        break
      } else {
        targetNode = targetNode.parentNode
      }
    }
  })
}

// Model

let board
let boardParams

// Model controls

function resetBoard(width, height, mineCount) {
  if (boardParams != null) {
    clearInterval(boardParams.secondCounterInterval)
  }
  boardParams = {
    width,
    height,
    mineCount,
    isTouched: false,
    isGameOver: false,
    isGameWon: false,
    revealedCount: 0,
    flaggedCount: 0,
    secondsElapsed: 0,
    secondCounterInterval: null
  }
  createEmptyBoard(width, height)
}

function createEmptyBoard(width, height) {
  board = []
  for (let x = 0; x < width; ++x) {
    board.push([])
    for (let y = 0; y < height; ++y) {
      board[x].push({
        value: 0,
        isMine: false,
        isRevealed: false,
        isFlagged: false
      })
    }
  }
}

function generateMines() {
  let minesPlaced = 0
  while (minesPlaced < boardParams.mineCount) {
    let x = Math.floor(Math.random() * boardParams.width)
    let y = Math.floor(Math.random() * boardParams.height)
    if (!board[x][y].isMine && !hasRevealedNeighbor(x, y)) {
      placeMine(x, y)
      ++minesPlaced
    }
  }
}

function placeMine(x, y) {
  board[x][y].isMine = true
  for (let cX = Math.max(x - 1, 0); cX <= Math.min(x + 1, board.length - 1); ++cX) {
    for (let cY = Math.max(y - 1, 0); cY <= Math.min(y + 1, board[cX].length - 1); ++cY) {
      board[cX][cY].value++
    }
  }
}

function startSecondCounter() {
  onTimeElapse(boardParams.secondsElapsed++)
  boardParams.secondCounterInterval = setInterval(() => onTimeElapse(boardParams.secondsElapsed++), 1000)
}

function stopSecondCounter() {
  clearInterval(boardParams.secondCounterInterval)
}

function clickField(x, y) {
  if (board[x][y].isFlagged) {
    return
  }

  revealField(x, y)

  if (!boardParams.isTouched) {
    boardParams.isTouched = true
    generateMines()
    startSecondCounter()
  }

  if (board[x][y].isMine) {
    boardParams.isGameOver = true
    stopSecondCounter()
    revealAllMines()
    onGameOver()
    return
  }

  if (isFieldEmpty(x, y)) {
    autoRevealFieldsAround(x, y)
  }
  
  checkIfGameIsWon()
}

function rightClickField(x, y) {
  if (!boardParams.isTouched) {
    return
  }
  if (board[x][y].isRevealed) {
    return
  }
  if (!board[x][y].isFlagged && boardParams.flaggedCount === boardParams.mineCount) {
    return
  }
  toggleFlagOnField(x, y)
  boardParams.flaggedCount += board[x][y].isFlagged ? 1 : -1
  onFlaggedCountChange()
}

function revealField(x, y) {
  board[x][y].isRevealed = true
  boardParams.revealedCount++
  onFieldValueChange(x, y)
}

function autoRevealFieldsAround(x, y) {
  for (let cX = Math.max(x - 1, 0); cX <= Math.min(x + 1, board.length - 1); ++cX) {
    for (let cY = Math.max(y - 1, 0); cY <= Math.min(y + 1, board[cX].length - 1); ++cY) {
      if (!board[cX][cY].isRevealed) {
        clickField(cX, cY)
      }
    }
  }
}

function revealAllMines() {
  for (let x = 0; x < board.length; ++x) {
    for (let y = 0; y < board[x].length; ++y) {
      if (board[x][y].isMine && !board[x][y].isRevealed) {
        revealField(x, y)
      }
    }
  }
}

function checkIfGameIsWon() {
  if (boardParams.revealedCount === board.length * board[0].length - boardParams.mineCount) {
    boardParams.isGameWon = true
    stopSecondCounter()
    flagAllMines()
    onGameWon()
  }
}

function flagAllMines() {
  for (let x = 0; x < board.length; ++x) {
    for (let y = 0; y < board[x].length; ++y) {
      if (board[x][y].isMine && !board[x][y].isFlagged) {
        toggleFlagOnField(x, y, true)
      }
    }
  }
}

function toggleFlagOnField(x, y, isFlagged) {
  board[x][y].isFlagged = isFlagged != null 
    ? isFlagged 
    : !board[x][y].isFlagged
  onFieldValueChange(x, y)
}

// Model helpers

function hasRevealedNeighbor(x, y) {
  for (let cX = Math.max(x - 1, 0); cX <= Math.min(x + 1, board.length - 1); ++cX) {
    for (let cY = Math.max(y - 1, 0); cY <= Math.min(y + 1, board[cX].length - 1); ++cY) {
      if (board[cX][cY].isRevealed) {
        return true
      }
    }
  }
  return false
}

function isFieldEmpty(x, y) {
  return !board[x][y].isMine && board[x][y].value === 0
}

// DOM references

const widthInput = document.querySelector('#width')
const heightInput = document.querySelector('#height')
const difficultyInput = document.querySelector('#difficulty')
const difficultyLabel = document.querySelector('#difficultyLabel')
const startButton = document.querySelector('#start')
const gameElement = document.querySelector('#game')
const boardElement = document.querySelector('#board')
const minesLeftElement = document.querySelector('#minesLeft')
const timeElapsedElement = document.querySelector('#timeElapsed')

// DOM helpers

function getFieldDisplayValue(x, y) {
  const field = board[x][y]
  if (!field.isRevealed) {
    return field.isFlagged ? flagChar : ' '
  }
  if (field.isMine) {
    return field.isFlagged ? correctFlagChar : bombChar
  }
  return field.value > 0 ? field.value : ' '
}

function getCoordinates(fieldElement) {
  return {
    x: parseInt(fieldElement.dataset.x),
    y: parseInt(fieldElement.dataset.y)
  }
}

function startNewGame(width, height, mineCount) {
  resetBoard(width, height, mineCount)
  resetBoardDOM()
  updateMinesLeftDOM()
  updateTimeElapsedDOM()
}

function disableAllFields() {
  boardElement.childNodes.forEach(column => column.childNodes.forEach(field => field.disabled = true))
}

// DOM event handlers

// Prevent opening context menu on board
function onBoardRightClick(event) {
  event.preventDefault()
}
boardElement.addEventListener('contextmenu', onBoardRightClick)

function onDifficultyValueChange() {
  let label = 'Difficulty: '
  switch (parseInt(difficultyInput.value)) {
    case 10: label += 'Beginner'
      break
    case 9: label += 'Easy'
      break
    case 8: label += 'Medium'
      break
    case 7: label += 'Hard'
      break
    case 6: label += 'Very hard'
      break
    case 5: label += 'Extreme'
      break
    case 4: label += 'Insane'
      break
    case 3: label += 'Impossible'
      break
  }
  difficultyLabel.innerText = label
}
difficultyInput.addEventListener('input', () => onDifficultyValueChange())

function onStartButtonClick() {
  const minWidth = parseInt(widthInput.min)
  const minHeight = parseInt(heightInput.min)
  const width = parseInt(widthInput.value)
  const height = parseInt(heightInput.value)
  const difficulty = parseInt(difficultyInput.value)
  if (width == '') {
    console.log('Please enter the board width')
    return
  }
  if (height == '') {
    console.log('Please enter the board height')
    return
  }
  if (width < minWidth || height < minHeight) {
    console.log(`Board size must be at least ${minWidth}x${minHeight}`)
    return
  }
  startNewGame(width, height, Math.floor(width * height / difficulty))
  showGame()
}
startButton.addEventListener('click', onStartButtonClick)

function onFieldClick(event, fieldElement) {
  if (boardParams.isGameWon || boardParams.isGameOver) {
    return
  }
  const { x, y } = getCoordinates(fieldElement)
  clickField(x, y)
}
delegateEvent(boardElement, 'click', '.field', onFieldClick)

function onFieldRightClick(event, fieldElement) {
  if (boardParams.isGameWon || boardParams.isGameOver) {
    return
  }
  const { x, y } = getCoordinates(fieldElement)
  rightClickField(x, y)
}
delegateEvent(boardElement, 'contextmenu', '.field', onFieldRightClick)

// Model event handlers

function onFieldValueChange(x, y) {
  const fieldElement = document.querySelector(`.field[data-x='${x}'][data-y='${y}']`)
  if (fieldElement != null) {
    updateFieldDOM(fieldElement)
  }
}

function onTimeElapse(secondsElapsed) {
  updateTimeElapsedDOM(secondsElapsed)
}

function onFlaggedCountChange() {
  updateMinesLeftDOM()
}

function onGameOver() {
  disableAllFields()
  updateMinesLeftDOM()
}

function onGameWon() {
  disableAllFields()
  updateMinesLeftDOM()
}

// DOM manipulators

function resetBoardDOM() {
  boardElement.replaceChildren()
  for (let x = 0; x < board.length; ++x) {
    const columnNode = document.createElement('div')
    columnNode.classList.add('column')
    for (let y = 0; y < board[x].length; ++y) {
      const fieldNode = document.createElement('button')
      fieldNode.classList.add('field')
      fieldNode.dataset.x = x
      fieldNode.dataset.y = y
      fieldNode.innerText = getFieldDisplayValue(x, y)
      columnNode.appendChild(fieldNode)
    }
    boardElement.appendChild(columnNode)
  }
}

function updateFieldDOM(fieldElement) {
  const x = fieldElement.dataset.x
  const y = fieldElement.dataset.y
  fieldElement.innerText = getFieldDisplayValue(x, y)
  if (board[x][y].isRevealed) {
    fieldElement.disabled = true
    fieldElement.classList.add('revealed')
  }
}

function updateMinesLeftDOM() {
  if (boardParams.isGameWon) {
    minesLeftElement.innerText = gameWonChar
  } else if (boardParams.isGameOver) {
    minesLeftElement.innerText = gameOverChar
  } else {
    minesLeftElement.innerText = `${(boardParams.mineCount - boardParams.flaggedCount)} ${bombChar}`
  }
}

function updateTimeElapsedDOM(secondsElapsed) {
  if (isNaN(secondsElapsed)) {
    secondsElapsed = 0
  }
  // Seconds
  let display = `${secondsElapsed % 60}`.padStart(2, '0')
  // Minutes
  display = `${Math.floor(secondsElapsed / 60) % 60}:${display}`
  // Hours
  if (secondsElapsed > 60 * 60) {
    display = display.padStart(2, '0')
    display = `${Math.floor(secondsElapsed / (60 * 60)) % 60}:${display}`
  }
  timeElapsedElement.innerText = `${display} ${clockChar}`
}

function showGame() {
  gameElement.classList.remove('hidden')
}

function hideGame() {
  gameElement.classList.add('hidden')
}

// Initializers

onDifficultyValueChange()